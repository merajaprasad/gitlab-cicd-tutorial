# GitLab CI-CD Pipeline Tutorial
### Below is basic CI-CD pipeline format

```bash
build:
 script:
  - atp update -y
  - apt install npm -y
  - npm install

deploy:
 script:
  - atp update -y
  - apt install nodejs -y
  - npm start
```
Note : Here we have one issue that which job will run 1st that is not mentioned on above pipeline.


### Defining the Stages into pipeline
```
stages:
 - build_stage
 - deploy_stage
build:
 stage: build_stage
 script:
  - atp update -y
  - apt install npm -y
  - npm install

deploy:
 stage: deploy_stage
 script:
  - apt update -y
  - apt install nodejs -y
  - npm start
```
Note: stages help to run the pipeline jobs one after one

### Defining Artifacts into pipeline
Note: Artifacts is used to transferor pass the information or dependencys or Reports from one job to another job.
```bash
stages:
 - build_stage
 - deploy_stage
build:
 stage: build_stage
 script:
  - atp update -y
  - apt install npm -y
  - npm install
  artifacts:
  paths:
   - node_modules
   - package-lock.json

deploy:
 stage: deploy_stage
 script:
  - apt update -y
  - apt install nodejs -y
  - npm start > /dev/null 2>&1 & 
```

### Run Node application in the background
```bash
npm start > /dev/null 2>&1 &
```


### Defining the OS Images/ Image Tagging into pipeline
Note: by-default Gitlab install ruby images to change, so we have to change the image as per requirement
```bash
stages:
 - build_stage
 - deploy_stage

build:
 stage: build_stage
 image: node
 script:
  - npm install

  artifacts:
  paths:
   - node_modules
   - package-lock.json

deploy:
 stage: deploy_stage
 image: node
 script:
  - npm start > /dev/null 2>&1 &
```

# Configure with AWS-EC2

## Runner
Note : Runner is an application that works with Gitlab CI-CD, is used to run the process or execute a service in a job in pipeline. tis can be installed on any platform.

### Install Runner on ec2-linux-2
```bash
curl -LJO https://gitlab-runner-downloads.s3.amazonaws.com/latest/rpm/gitlab-runner_amd64.rpm
sudo rpm -Uvh gitlab-runner_amd64.rpm

sudo gitlab-runner start
```

### Register Runner with EC2
```bash
sudo gitlab-runner register --url https://gitlab.com/ --registration-token GR1348941Wg5zz_86Qepy8vUjAohB # enter your own token
```
Note : Enter the following details as mentioned here
`url` - same (hit Enter).
`token` - same (hit enter).
`Description` - medevops (give a description of your job).
`Tag` - server, ec2 (give tag of your server).
`Executer` - Shell (give executer name 'shell').

### Verify Runner Status
```bash
sudo gitlab-runner status
```

## Run Applications on Docker-Container
Prerequites
- Ec2 Server
- Docker
- Add gitlab Runner in Docker Group

```bash
sudo usermod -aG docker gitlab-runner
```
### Pipeline
```bash
stages:
 - build_stage
 - deploy_stage

build:
 stage: build_stage
 script:
  - docker --version
  - docker build -t pyapp . # this will run docker file

 tags:
  - ec2
  - server

  artifacts:
  paths:
   - node_modules
   - package-lock.json

deploy:
 stage: deploy_stage
 script:
  - docker run -d --name pythoncontainer -p 80:8080 pyapp
 tags:
  - ec2
  - server
```

## Variables in CI-CD pipeline
GitLab support Three Types of variables
- Predefined variable
- Custom variable
- Secret variable

#### 1. Predefine Variable
predefine variables are inbuild variables which is provided by gitlab. Here is the [gitlab Predefine Variable list](https://docs.gitlab.com/) from gitlab documentations.

#### 2. Custom variable

To create a custom variable we have user Keyword "variables" and under this we have to define variable names in key-value pair.

```bash
variables:
 Name: "cloudChamp"
 Message: "please"

job_name:
 script:
  - echo "Hey $Message subscribe to $Name"

```
Note: Here two variable has created and also calling the vaiables using ($) symbol inside the job.

#### 3. Secret variable
This varaible store information, like password and secret keys of dockerhub, jenkins, terraform etc, that you can use in job scripts.



## Push container to Docker Hub
prerequites
- create secret variable for dockerhub credintials. used name DOCKER_USERNAME and DOCKER_TOKEN

```bash

stages:
 - build_stage
 - deploy_stage

build:
 stage: build_stage
 script:
  - docker --version
  - docker build -t pyapp . # this will run docker file

 tags:
  - ec2
  - server

 artifacts:
  paths:
    - node_modules
    - package-lock.json

deploy:
 stage: deploy_stage
 script:
  - docker run -d --name pythoncontainer -p 80:8080 pyapp
  - docker login -u $DOCKER_USERNAME -p $DOCKER_TOKEN
  - docker tag pyapp $DOCKER_USERNAME/pyapp      # Tagging the Image
  - docker push $DOCKER_USERNAME/pyapp 
 tags:
  - ec2
  - server

```

## Keywords in GitLab

keyword helps to enhance the cicd pipeline. Here is the [gitlab keyword list](https://docs.gitlab.com/ee/ci/yaml/) which you can follow to know more details about keywords.


## gitlab Analyzer

## gitlab CI Lint
gitlab CI Lint is default syntax validator tool checks the syntax of GitLab CI/CD configuration, including configuration added with the includes keyword. If you want to test the validity of your GitLab CI/CD configuration before committing the changes, you can use the CI Lint tool.

### gitlab Environments


