# Software Name : XYZ-App
# SPDX-FileCopyrightText: Copyright (c) Orange SA
# SPDX-License-Identifier: Apache-2.0
#
# This software is distributed under the  Apache-2.0
# See the "LICENSES" directory for more details.
#
# Authors:
# - Raja Prasad <rajaprasad.rp731@gmail.com>

---

stages:
  - install_tools
  - test
  - build
  - artifact_publish
  - dockerize
  - image_scan
  - smoke_test
  - deploy


variables:
  APP_NAME: "java-app"
  IMAGE_TAG: "${CI_PIPELINE_IID}"
  DOCKER_USR: "merajaprasd"

default:
  tags:
    - dev

install_tools:
  stage: install_tools
  script:
    - echo "Installing required tools..."

mvn_compile:
  stage: test
  script:
    - echo "all the test job will execute in this stage"
    - mvn compile

unit_tests:
  stage: test
  script:
    - echo "all the test job will execute in this stage"
    - mvn test

static_analysis:
  stage: test
  script:
    - echo "Running OWASP Dependency-Check..."
    - dependency-check --scan ./ --format XML -o .
    - echo "Publishing Dependency-Check Report..."
  artifacts:
    paths:
      - dependency-check-report.xml

trivy_fs_scan:
  stage: test
  script:
    - echo "Scanning file system for vulnerabilities using Trivy..."
    - trivy fs --format table -o fs.html .
  artifacts:
    paths:
      - fs.html

sonarqube-check:
  stage: test
  image: 
    name: sonarsource/sonar-scanner-cli:latest
  variables:
    SONAR_USER_HOME: "${CI_PROJECT_DIR}/.sonar"
    GIT_DEPTH: "0" 
  cache:
    key: "${CI_JOB_NAME}"
    paths:
      - .sonar/cache
  script: 
    - sonar-scanner
  allow_failure: true
  only:
    - main


mvn_build:
  stage: build
  script:
    - echo "Building the Java application and packaging as a JAR..."
    - mvn clean package
  artifacts:
    paths:
      - target/*.jar
    expire_in: 1 week


nexus_publish:
  stage: artifact_publish
  script:
    - echo "Publishing artifact to Nexus..."
    - mvn deploy -s .m2/settings.xml

build_image:
  stage: dockerize
  script:
    - echo "Building Docker Image..."
    - mvn clean package
    - docker build -t ${DOCKER_USR}/${APP_NAME}:${IMAGE_TAG} .

push_image:
  stage: dockerize
  before_script:
    - docker login -u $DOCKER_USERNAME -p $DOCKER_TOKEN
  script:
    - echo "Pushing Docker image to DockerHub..."
    - docker push ${DOCKER_USR}/${APP_NAME}:${IMAGE_TAG}

trivy_image_scan:
  stage: image_scan
  script:
    - echo "Scanning Docker image with Trivy..."
    - trivy image --format table -o trivyresults.html ${DOCKER_USR}/${APP_NAME}:${IMAGE_TAG}
  artifacts:
    paths:
      - trivyresults.html


smoke_test:
  stage: smoke_test
  script:
    - echo "Smoke Test the Image"
    - docker run -d --name smokerun -p 8080:8080 --rm ${DOCKER_USR}/${APP_NAME}:${IMAGE_TAG}
    - netstat -tuln | grep ":8080" || (echo "Service not running on port 8080" && exit 1)


# Before deploy into kubernetes cluster add config file into gitlab secrets.
# decrypt the config file
# echo -n 'configfile-details' | base64

kubernetes_deploy:
  stage: deploy
  variables:
    KUBECONFIG_PATH: /home/ubuntu/.kube/config
  before_script:
    - mkdir -p $(dirname "KUBECONFIG_PATH")
    - echo "KUBECONFIG_CONTENT" | base64 -d > "$KUBECONFIG_PATH"
    - export KUBECONFIG="$KUBECONFIG_PATH"
  script:
    - echo "Deploying Application on kubernetes"
    - kubectl apply -f deployment.yml


